.. _api:

============
Toolkit API
============


.. module:: IIRrational

---------------------------
Linear Rational Disc Stage
---------------------------

.. autoclass:: IIRrational.RDF.RationalDiscFilter
   :inherited-members:


-------------------------------------------
Nonlinear flexible parameterization fitting
-------------------------------------------

.. autoclass:: IIRrational.MRF.MultiReprFilterZ
   :inherited-members:

-------------------------------------------
Root Parameterization Codings
-------------------------------------------

.. automodule:: IIRrational.codings
   :inherited-members:

