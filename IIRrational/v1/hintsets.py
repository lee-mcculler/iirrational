# -*- coding: utf-8 -*-
"""
"""
from __future__ import division, print_function, unicode_literals
#import warnings


exact_data = dict(
    resavg_RthreshOrdDn = None,
    resavg_RthreshOrdUp = None,
    resavg_RthreshOrdC  = None,
    resavg_EthreshOrdDn = 1e-2,
)

quiet = dict(
    log_print = False,
)


verbose = dict(
    log_print    = True,
    optimize_log = True,
)
