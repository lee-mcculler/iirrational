"""
"""
from __future__ import division, print_function, unicode_literals

version_info = (2, 5, 1)
version = '.'.join(str(v) for v in version_info)
__version__ = version

