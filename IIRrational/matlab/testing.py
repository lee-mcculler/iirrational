"""
"""
from __future__ import division, print_function, unicode_literals

from IIRrational.testing import IIRrational_data

IIRrational_data._msurrogate_MT = False

__all__ = [
    'IIRrational_data',
]
