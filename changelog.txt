
version 2.5.0:

This version mostly reflects the tune-ups that have been in master for a while.
In addition, I have added some more options to the data2filter and
IIRrationalV2fit program to make iterative workflow a bit more efficient.

poles and zeros output:

  First, IIRrationalV2fit now outputs its fit poles and zeros in the form of
  arguments to feed into it to improve the next fit. This will allow a second
  fit to start from the first one. You can also then easily edit them to remove
  extraneous RHP zeros and features.

  example, on the oplev demo, it now outputs:
  %%% add or adjust the following poles and zeros to bootstrap the fit and refine. Disable this message with --no-refine
  --zeros=-1.13695+25.28086j,-1.13695-25.28086j,-33.26677+84.41108j,-33.26677-84.41108j,-17.35178+32.25408j,-17.35178-32.25408j,69.42532+96.10796j,69.42532-96.10796j,-115.73474+180.14762j,-115.73474-180.14762j
  --poles=-0.83878+25.65321j,-0.83878-25.65321j,-20.47833+79.09176j,-20.47833-79.09176j,-0.12502,-7.50335,-17.72005,-111.68572,-299.38060
  %%%

  so just cut/paste that into the program command line call and it will start
  with a pretty good fit. This will help it prioritize remaining small features
  if it misses them the first round.

Coherence:
  I've also added an input for coherence, using the '-C' command line
  to list arrays containing coherence data. Each array is converted to SNR and
  then they are combined (reducing the SNR). This also works in the function
  call with the 'COH' or 'coherence' arguments.

Inverses:
  the "--inverse" flag now takes xfer = 1/xfer to fit inverses without adjusting the source data

Selecting ranges:
  --F_max_Hz <high>, --F_min_Hz <low>
  allows one to select a subset of the data based on frequency range

Downsampling!:
  the --downsample N argument causes the data to be downsampled.
  --downsample_type selects how the density is determined, from 'log', 'linear',
  'loglin' (default). loglin uses log spacing for the first 3/4 of the data and
  linear for the remainder.

  These arguments do weighted averaging of the data, so data is not discarded
  and the SNR/weights are respected.

  Combining this with the pole/zero output allows one to do a quick downsampled
  iteration and then follow it with a slower full-data refinement.

SNR estimation: the sliding-average estimator of the SNR that is used when SNR
    or COH arrays are not given is now improved a little. It is now better at
    handling magnitude variations and now interpreting them as noise. Internally
    it is now using log(abs()) and np.angle rather than the real/imag parts.

SNR max and min: you can cut away data with too low of SNR using --SNR_min,
    useful when adding SNR or coherence arrays. You can also clip the maximum
    SNR which can help some fits. This is done for you to improve fitting, but you
    can clip it further if desired.

modes:
   I added the modes 'full2x' and 'rational2x' to run the rational fitter
   twice. The first iteration only uses order 8 and only some reduction. The
   second attempt uses the standard order. This is to do the 2-round workflow
   for you in a slightly faster way. Not sure if it is preferable so I did not
   make it the default.
