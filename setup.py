#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division, print_function, unicode_literals
from setuptools import find_packages, setup
import setup_helper


version = '2.5.1'

cmdclass = setup_helper.version_checker(version, 'IIRrational')

setup_args = dict(
    name             = 'IIRrational',
    version          = version,
    url              = 'https://github.com/mccullerlp/IIRrational',
    author           = 'Lee McCuller',
    author_email     = 'Lee.McCuller@gmail.com',
    description      = "Fast IIR System Identification (rational function fitting)",
    license          = 'Copyright 2018 Lee McCuller',
    install_requires = [
        'numpy',
        'scipy',
        'declarative>=1.2.0',
        'matplotlib',
    ],
    extras_require = {
        'cmd' : ['ipython', 'h5py', 'PyYAML'],
        'matlab' : ['msurrogate'],
    },
    entry_points={
        'console_scripts': ['IIRrationalV2fit = IIRrational.v2.__main__:main [cmd]']
    },
    packages = find_packages(
        exclude = ['docs', 'IIRrational_test_data', 'matlab'],
    ),
    include_package_data = True,
    zip_safe = False,
    keywords = 'SystemId, IIR, FIR, Z-domain, S-domain, rational, transfer-function, fitting, optimization',
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Intended Audience :: Developers',
        'Topic :: Scientific/Engineering',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
    ],
)

if __name__ == "__main__":
    setup(
        cmdclass = cmdclass,
        **setup_args
    )
