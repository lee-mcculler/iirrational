"""
Contains setup functions for test data, these are returned in a declarative.Bunch dictionary, with some annotation about the number of data sets
"""
from os import path

import declarative
from declarative.bunch.hdf_deep_bunch import HDFDeepBunch

from IIRrational.testing import utilities


dataset_full = None

data_folder = path.join(path.split(__file__)[0], 'data')

def load_data(max_F_Hz = None):
    global dataset_full
    if dataset_full is None:
        data_b = HDFDeepBunch(path.join(data_folder, 'quad', '2017-08-28_0900_L1SUSITMX_M0_V_WhiteNoise.h5'))
        ch1 = 'L1:SUS-ITMX_M0_DAMP_P_IN1_DQ'
        ch2 = 'L1:SUS-ITMX_M0_TEST_V_EXC'

        dataset = declarative.Bunch(
            F_Hz = data_b.FHz.flatten(),
            data = data_b.XFER[ch1][ch2],
            SNR = data_b.XFER_SNR_EST[ch1][ch2],
        )
        dataset_full = dataset
    if max_F_Hz is None:
        return dataset_full
    else:
        select = (dataset_full.F_Hz > 0) & (dataset_full.F_Hz < max_F_Hz)
        return declarative.Bunch(
            F_Hz               = dataset_full.F_Hz[select],
            data               = dataset_full.data[select],
            SNR                = dataset_full.SNR[select],
        )


description = """
LIGO Data from the quadruple suspension top mass. This is a cross term from pitch to vertical. This is a transfer which has a larg number of features and large rolloff at the end.
The data at the end from 10Hz to 20Hz gets low SNR and affects the fit.
"""

def quad_M0_P2V_10Hz(**kwargs):
    """
    Cleanest dataset only to 10Hz
    """
    dset = load_data(max_F_Hz = 10)
    #from a particularly nice fit. Probably could be yet lower order though
    ZPKz = ([
        0.99982876+1.73477051e-03j,  0.99982876-1.73477051e-03j,
        0.99998537+4.63038989e-03j,  0.99998537-4.63038989e-03j,
        0.99976543+6.16948899e-03j,  0.99976543-6.16948899e-03j,
        0.99996723+7.55503530e-03j,  0.99996723-7.55503530e-03j,
        0.99975474+8.99710033e-03j,  0.99975474-8.99710033e-03j,
        0.95471082+3.28858132e-06j,  0.95471082-3.28858132e-06j,
        0.99979547+9.57816978e-03j,  0.99979547-9.57816978e-03j,
        0.99987349+1.15530870e-02j,  0.99987349-1.15530870e-02j,
        1.00016441+1.72446722e-03j,  1.00016441-1.72446722e-03j,
        1.00013958+6.16378712e-03j,  1.00013958-6.16378712e-03j,
        1.00014628+9.73089752e-03j,  1.00014628-9.73089752e-03j,
        0.99992968+1.52216346e-02j,  0.99992968-1.52216346e-02j
    ], [
        0.99999470+0.00167121j,  0.99999470-0.00167121j,
        0.99999459+0.0017346j ,  0.99999459-0.0017346j ,
        0.99998464+0.00425596j,  0.99998464-0.00425596j,
        0.99998309+0.00509983j,  0.99998309-0.00509983j,
        0.99997479+0.00622848j,  0.99997479-0.00622848j,
        0.99996521+0.00699364j,  0.99996521-0.00699364j,
        0.99996535+0.0075046j ,  0.99996535-0.0075046j ,
        0.99994822+0.00861778j,  0.99994822-0.00861778j,
        0.99994781+0.00951204j,  0.99994781-0.00951204j,
        0.97848568+0.01846217j,  0.97848568-0.01846217j,
        0.99993682+0.01034654j,  0.99993682-0.01034654j,
        0.99993047+0.01120527j,  0.99993047-0.01120527j,
        0.99986348+0.01581266j,  0.99986348-0.01581266j
    ],
        3.7445883326764972e-14
    )
    return utilities.generator_autofill(
        F_Hz               = dset.F_Hz,
        data               = dset.data,
        SNR                = dset.SNR,
        F_nyquist_Hz       = 1024,
        bestfit_ZPK_z      = ZPKz,
        **kwargs
    )


def quad_M0_P2V_15Hz(**kwargs):
    """
    dataset to 15Hz with some low SNR points
    """
    dset = load_data(max_F_Hz = 15)
    return utilities.generator_autofill(
        F_Hz               = dset.F_Hz,
        data               = dset.data,
        SNR                = dset.SNR,
        F_nyquist_Hz       = 1024,
        **kwargs
    )


def quad_M0_P2V_20Hz(**kwargs):
    """
    dataset to 20Hz with large amount of low SNR points
    """
    dset = load_data(max_F_Hz = 20)
    return utilities.generator_autofill(
        F_Hz               = dset.F_Hz,
        data               = dset.data,
        SNR                = dset.SNR,
        F_nyquist_Hz       = 1024,
        **kwargs
    )


def quad_M0_P2V(sN = 0, iN = 0, **kwargs):
    """
    Accesses datasets by instance number
    """
    return [
        quad_M0_P2V_10Hz,
        quad_M0_P2V_15Hz,
        quad_M0_P2V_20Hz,
    ][sN](
        sN = sN,
        iN = iN,
        **kwargs
    )


datasets = dict(
    quad_M0_P2V_10Hz = utilities.make_description(
        generator = quad_M0_P2V_10Hz,
        instances = 1,
        description = description,
    ),
    quad_M0_P2V_15Hz = utilities.make_description(
        generator = quad_M0_P2V_15Hz,
        instances = 1,
        description = description,
    ),
    quad_M0_P2V_20Hz = utilities.make_description(
        generator = quad_M0_P2V_20Hz,
        instances = 1,
        description = description,
    ),
    quad_M0_P2V = utilities.make_description(
        generator   = quad_M0_P2V,
        sets        = 3,
        instances   = 1,
        description = description,
    ),
)

