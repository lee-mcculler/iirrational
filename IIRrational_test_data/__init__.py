# -*- coding: utf-8 -*-
"""
"""
from __future__ import division, print_function, unicode_literals

from IIRrational import testing

from . import quad
from . import L2P
from . import misc


datasets = {}
datasets.update(quad.datasets)
datasets.update(L2P.datasets)
datasets.update(misc.datasets)
datasets.update(testing.datasets)


def IIRrational_data_dev(
    name,
    set_num = 0,
    instance_num = 0,
    **kwargs
):
    return datasets[name].generator(
        sN = set_num,
        iN = instance_num,
        **kwargs
    )

