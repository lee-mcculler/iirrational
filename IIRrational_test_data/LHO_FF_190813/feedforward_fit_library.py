import sys
sys.path.append('/ligo/cds/lho/h1/anaconda/anaconda2/envs/cragenv/lib/python2.7/site-packages/')

import os, sys, time
sys.path.append(os.path.expanduser('/ligo/home/craig.cahillane/Git/labutils/python_40mutils/'))
import numpy as np

import matplotlib as mpl 
import matplotlib.pyplot as plt 
sys.path.append(os.path.expanduser('/ligo/home/craig.cahillane/utils/'))

import scipy.constants as scc
import scipy.special as scp
from scipy.interpolate import interp1d

import subprocess

import scipy.signal as sig

import dtt2hdf

sys.path.append('/ligo/home/hang.yu/Desktop/pyComm/')

import IIRrational.v2


# ####################################

def MakeComplexPair(f,Q):
    # Make a complex pair of zeros or poles, given the frequency in Hz and quality factor

    omega = 2*np.pi*f
    aa = omega / (2*Q)
    bb = np.sqrt(omega**2 - aa**2)
    
    pair = np.array([aa+bb*1j, aa-bb*1j])/(2*np.pi) # in matlab, I use with -2pi*pair
    
    return pair



# ####################################

def plot_DTT_data(ff, tf, coh, coh_cutoff=0.9, ff_cutoff=None, title='TF'):   
    # Function that will plot the data that has been extracted from DTT 
    
    # Find indices where the coherence in both measurements is above threshold
    keepIdx = np.intersect1d(np.argwhere(coh > coh_cutoff)[:,0], np.argwhere(coh > coh_cutoff)[:,0])
    
    # If restricting frequency range, find indices to keep
    if ff_cutoff is not None:
        # If we are restricting our fit to some freq range
        if len(ff_cutoff) == 1:
            keepIdx = np.intersect1d(keepIdx, np.argwhere(ff > ff_cutoff)) 
        else:
            keepIdx = np.intersect1d(keepIdx, np.argwhere(ff > ff_cutoff[0]))
            keepIdx = np.intersect1d(keepIdx, np.argwhere(ff < ff_cutoff[1]))
                
    
    fig = plt.figure(figsize=(16,18))
    s1 = fig.add_subplot(311)
    s2 = fig.add_subplot(312)
    s3 = fig.add_subplot(313)

    s1.loglog(ff, np.abs(tf))
    s1.loglog(ff[keepIdx], np.abs(tf[keepIdx]))

    s2.semilogx(ff, 180/np.pi*np.angle(tf))
    s2.semilogx(ff[keepIdx], 180/np.pi*np.angle(tf[keepIdx]))

    s3.semilogx(ff, coh)
    s3.semilogx(ff[keepIdx], coh[keepIdx])

    s1.set_title(title)

    s1.grid(which='minor', ls='--')
    s2.grid(which='minor', ls='--')
    s3.grid(which='minor', ls='--')
    
    s3.set_xlabel('Frequency [Hz]')
    
    s1.set_ylabel('Mag')
    s2.set_ylabel('Phase [deg]')
    s3.set_ylabel('Coherence')
    
    plt.show()

# ####################################

def plotFit(ff, FF_TF, fit, dof):
    fig = plt.figure(figsize=(16,30))
    s1 = fig.add_subplot(411)
    s2 = fig.add_subplot(412)
    s3 = fig.add_subplot(413)
    s4 = fig.add_subplot(414)
    
    ff_plot = np.logspace(np.minimum(-1,np.log10(ff[1])), np.maximum(3,np.log10(ff[-1])),1000)
    
    __, fit_resp_plot = sig.freqresp((fit.as_scipy_signal_ZPKsw()), 2.*np.pi*ff_plot)
    __, fit_resp = sig.freqresp((fit.as_scipy_signal_ZPKsw()), 2.*np.pi*ff)
    
    s1.loglog(ff, np.abs(FF_TF), '.', label='Meas feedforward TF')   
    s1.loglog(ff_plot, np.abs(fit_resp_plot), label='Fit to TF')  

    s2.semilogx(ff, 180/np.pi*np.angle(FF_TF), '.')
    s2.semilogx(ff_plot, 180/np.pi*np.angle(fit_resp_plot))

    s3.semilogx(ff, np.abs(FF_TF / fit_resp), label='Residual') 
    s4.semilogx(ff, 180/np.pi*np.angle(FF_TF / fit_resp), label='Residual') 


    s1.legend()
    s3.legend()

    s3.set_ylim([0.8,1.2])
    s4.set_ylim([-10,10])

    s1.set_title('%s FF'%dof)
    s1.set_ylabel('TF Mag')
    s1.set_xlabel('Freq [Hz]')

    s2.set_ylabel('TF Phase')
    s2.set_xlabel('Freq [Hz]')

    s3.set_ylabel('Residual Mag')
    s3.set_xlabel('Freq [Hz]')

    s4.set_ylabel('Residual Phase')
    s4.set_xlabel('Freq [Hz]')

    s1.grid(which='minor', ls='--')
    s2.grid(which='minor', ls='--')
    s3.grid(which='minor', ls='--')
    s4.grid(which='minor', ls='--')

# ####################################

#17July2019, JCD: Not sure if this really works properly yet.  Need to check
def plotDARM(fit, start_t, FF2D_XFER, LSCdof, duration=256):
    # Take some data, plot the effect of the new FF filters
    print('going to make a plot')
    
    LSCdata  = TimeSeries.get('H1:LSC-%s_OUT_DQ'%LSCdof, start_t, start_t+duration)
    DARMdata = TimeSeries.get('H1:LSC-DARM_IN1_DQ',      start_t, start_t+duration)
    
    DARM_asd = DARMdata.asd(8,4)
    LSC_asd = LSCdata.asd(8,4)
    freq = np.array(LSC_asd.frequencies)
    
    freq_idx = np.intersect1d(np.argwhere(freq > 1), np.argwhere(freq < 900))
    #ff_idx = np.intersect1d(np.argwhere(FF2D_XFER.FHz > 1), np.argwhere(FF2D_XFER.FHz < 900))

    #FF2D_tf = np.interp(freq[freq_idx], FF2D_XFER.FHz[ff_idx], (FF2D_XFER.xfer[ff_idx]))
    FF2D_tf = np.interp(freq[freq_idx], FF2D_XFER.FHz, FF2D_XFER.xfer)
    
    __, FF_fit = sig.freqresp((fit.choice_export_dict()['zpk']['z'], fit.choice_export_dict()['zpk']['p'], fit.choice_export_dict()['zpk']['k']), freq[freq_idx])
    Filtered_asd = LSC_asd[freq_idx] * (FF_fit) * (FF2D_tf) # need the FF fit, as well as the actuation from ITMs to DARM.  Use direct measured for the actuation so we're seeing errors in the fit.
    SubtractedDARM_asd = (DARM_asd[freq_idx] - Filtered_asd) # adding filtered data and DARM
    
    plt.figure(figsize=[30,20])
    ax = plt.gca(xlabel='Frequency [Hz]')#, xlim=[1,400])#, ylim=[1e-4,1e0])
    ax.loglog(freq[freq_idx],     np.abs(Filtered_asd.value),      label='filt %s'%LSCdof)
    ax.loglog(freq[freq_idx],     np.abs(DARM_asd.value[freq_idx]),           label='Raw DARM')
    ax.loglog(freq[freq_idx],     np.abs(SubtractedDARM_asd.value), label='DARM - filt %s'%LSCdof)
    #ax.loglog(freq,      LSC_asd.value,label='Raw %s'%LSCdof)

    ax.legend()

# ####################################


# 17 July 2019: No longer using this FitFunc - it is v1 of IIRrational, we now use v2.  JCD.
'''def FitFunc(ff, FF_TF, LSC_to_Darm_coh, LSCFF_to_Darm_coh, coh_cutoff=0.9, ff_cutoff=None, fit_order=12, zpk_dict=None, roundNumber=1):
    # ff = frequency vector
    # FF_TF = vector to fit
    # LSC_to_Darm_coh = coherence of one measurement from DTT
    # LSCFF_to_Darm_coh = coherence of other measurement from DTT
    # coh_cutoff = coherence threshold; only keep points where both measurements have coherence above this value. Default = 0.9
    # ff_cutoff: Used for restricting the frequency range of the fit. Can be None (default), one item (only keep freqs above that value), or 2 items (only keep freqs between those values)
    # fit_order: initial guess for order of iirrational fit. Default=12
    
    # Find indices where the coherence in both measurements is above threshold
    keepIdx = np.intersect1d(np.argwhere(LSC_to_Darm_coh > coh_cutoff)[:,0], np.argwhere(LSCFF_to_Darm_coh > coh_cutoff)[:,0])
    
    # If restricting frequency range, find indices to keep
    if ff_cutoff is not None:
        # If we are restricting our fit to some freq range
        if len(ff_cutoff) == 1:
            keepIdx = np.intersect1d(keepIdx, np.argwhere(ff > ff_cutoff)) 
        else:
            keepIdx = np.intersect1d(keepIdx, np.argwhere(ff > ff_cutoff[0]))
            keepIdx = np.intersect1d(keepIdx, np.argwhere(ff < ff_cutoff[1]))
            
    ff_keep = ff[keepIdx]
    FF_TF_keep = FF_TF[keepIdx]
    
    print 'Number of points with coherence above {}: {}'.format(coh_cutoff, len(keepIdx))
    
    # Iterate the fitting rounds that have already been done, and apply those filters to get a residual for the next round fit
    if zpk_dict is not None:
        for rounds, zpkvals in zpk_dict.items():
            if int(rounds[-1]) < roundNumber:
                print 'Applying filter from ' + rounds
                __, FFTFfit_keep       = sig.freqresp((zpkvals['zz'], zpkvals['pp'], zpkvals['kk']), 2.*np.pi*ff_keep)
                FF_TF_keep = FF_TF_keep / FFTFfit_keep
    
    
    print 'Starting iirrational fit'
    # Do the iirrational fitting
    f_nyq = 2**21 #just make huge
    fit=iirrational.v1.data2filter(
        data=FF_TF_keep, 
        F_Hz=ff_keep, 
        order_initial=fit_order, 
    #     SNR=goodCoh * np.abs(1./goodff), 
        F_nyquist_Hz=f_nyq, 
        hints={'log_print':True}
    )
    
    zpk_s = pyctrl.d2c(zpk_z=fit.fitter.ZPK, fs=2.*f_nyq, method='matched', f_match=0.1) # takes discrete "z" domain poles and zeros from Lee's fit and converts to continuous Laplace "s" domain
    zz, pp, kk = zpk_s # rad/s
    
    # Can also get a lower-order answer from iirrational, but we're not using it.  But, remember that it exists
    # zpk_l = pyctrl.d2c(zpk_z=fit.fitter_loword.ZPK, fs=2.*f_nyq, method='matched', f_match=0.1) # get lower order "loword" version of fitted answer
    
    return zz, pp, kk
'''
