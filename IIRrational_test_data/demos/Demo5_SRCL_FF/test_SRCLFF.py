# -*- coding: utf-8 -*-
"""
"""
from __future__ import division, print_function, unicode_literals, absolute_import

import pytest
import numpy as np
from os import path

from IIRrational.pytest import (  # noqa: F401
    tpath_join, plot, pprint, tpath, tpath_preclear
)
from IIRrational.v2.__main__ import IIRrationalV2fit


def test_SRCL_FF1(tpath_join, tpath_preclear, pprint):
    dname =  'SRCL_FF.mat'
    dfile = path.join(path.split(__file__)[0], dname)
    ofile = tpath_join(path.splitext(dname)[0] + '_fit.mat')
    pfile = tpath_join(path.splitext(dname)[0] + '_fit.pdf')
    args = "{} {} -D '' --plot_fit {} --mode full2x --downsample 500 --downsample_type=linlog --never_unstable_poles --choose baseline --F_max_Hz 300 --F_min_Hz 5".format(dfile, ofile, pfile)
    pprint('calling with args')
    pprint(args)
    IIRrationalV2fit(args)
    return

def test_SRCL_FF2(tpath_join, tpath_preclear, pprint):
    dname =  'SRCL_FF.mat'
    dfile = path.join(path.split(__file__)[0], dname)
    ofile = tpath_join(path.splitext(dname)[0] + '_fit.mat')
    pfile = tpath_join(path.splitext(dname)[0] + '_fit.pdf')
    args = "{} {} -D '' --plot_fit {} --mode full --F_boost_Hz 20-30 --downsample 200 --downsample_type=linlog --never_unstable_poles --choose baseline --F_max_Hz 300 --F_min_Hz 5".format(dfile, ofile, pfile)
    pprint('calling with args')
    pprint(args)
    IIRrationalV2fit(args)
    return

