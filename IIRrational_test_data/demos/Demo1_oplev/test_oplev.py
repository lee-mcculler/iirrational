# -*- coding: utf-8 -*-
"""
"""
from __future__ import division, print_function, unicode_literals, absolute_import

import pytest
import numpy as np
from os import path

from IIRrational.pytest import (  # noqa: F401
    tpath_join, plot, pprint, tpath, tpath_preclear
)
from IIRrational.v2.__main__ import IIRrationalV2fit


def test_oplev_fit1(tpath_join, tpath_preclear, pprint):
    dname =  'OplevPlant_invs.mat'
    dfile = path.join(path.split(__file__)[0], dname)
    ofile = tpath_join(path.splitext(dname)[0] + '_fit.mat')
    pfile = tpath_join(path.splitext(dname)[0] + '_fit.pdf')
    args = "{} {} -D ap.ey -Xc plant_inv -F ff --plot_fit {} --mode full --never_unstable_poles --choose baseline --order 10 ".format(dfile, ofile, pfile)
    pprint('calling with args')
    pprint(args)
    IIRrationalV2fit(args)
    return


def test_oplev_fit_refine(tpath_join, tpath_preclear, pprint):
    dname =  'OplevPlant_invs.mat'
    dfile = path.join(path.split(__file__)[0], dname)
    ofile = tpath_join(path.splitext(dname)[0] + '_fit.mat')
    pfile = tpath_join(path.splitext(dname)[0] + '_fit.pdf')
    args = "{} {} -D ap.ey -Xc plant --inverse -F ff --plot_fit {} --mode full --never_unstable_poles --choose baseline --order 10 --zeros=-0.16692+1.63268j,-0.16692-1.63268j,-0.01822+0.53630j,-0.01822-0.53630j,-0.06235+1.43770j,-0.06235-1.43770j,-0.11582+2.70648j,-0.11582-2.70648j --poles=-0.29343+1.99825j,-0.29343-1.99825j".format(dfile, ofile, pfile)
    pprint('calling with args')
    pprint(args)
    IIRrationalV2fit(args)
    return

def test_oplev_fit_full2x(tpath_join, tpath_preclear, pprint):
    dname =  'OplevPlant_invs.mat'
    dfile = path.join(path.split(__file__)[0], dname)
    ofile = tpath_join(path.splitext(dname)[0] + '_fit.mat')
    pfile = tpath_join(path.splitext(dname)[0] + '_fit.pdf')
    args = "{} {} -D ap.ey -Xc plant --inverse -F ff --plot_fit {} --mode full2x --never_unstable_poles --choose baseline".format(dfile, ofile, pfile)
    pprint('calling with args')
    pprint(args)
    IIRrationalV2fit(args)
    return

#def test_l2p_fit_COH(tpath_join, tpath_preclear, pprint):
#    dname =  'bs_m2l2p_meas_06232020.mat'
#    dfile = path.join(path.split(__file__)[0], dname)
#    ofile = tpath_join(path.splitext(dname)[0] + '_fit.mat')
#    pfile = tpath_join(path.splitext(dname)[0] + '_fit.pdf')
#    args = '{} {} -Xc xfer -F F_Hz --plot_fit {} -C "coh.L2P, cohP2P" --mode full --choose baseline --never_unstable_poles --F_max_Hz 2 --order 20 --poles=-2,-2'.format(dfile, ofile, pfile)
#    pprint('calling with args')
#    pprint(args)
#    IIRrationalV2fit(args)
#    return
