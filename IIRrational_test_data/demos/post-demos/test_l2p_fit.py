# -*- coding: utf-8 -*-
"""
"""
from __future__ import division, print_function, unicode_literals, absolute_import

import pytest
import numpy as np
from os import path

from IIRrational.pytest import (  # noqa: F401
    tpath_join, plot, pprint, tpath, tpath_preclear
)
from IIRrational.v2.__main__ import IIRrationalV2fit


def test_l2p_fit1(tpath_join, tpath_preclear, pprint):
    dname =  'bs_m2l2p_meas_06232020.mat'
    dfile = path.join(path.split(__file__)[0], dname)
    ofile = tpath_join(path.splitext(dname)[0] + '_fit.mat')
    pfile = tpath_join(path.splitext(dname)[0] + '_fit.pdf')
    args = "{} {} -Xc xfer -F F_Hz --plot_fit {} --mode full2x --never_unstable_poles".format(dfile, ofile, pfile)
    pprint('calling with args')
    pprint(args)
    IIRrationalV2fit(args)
    return


def test_l2p_fit_COH(tpath_join, tpath_preclear, pprint):
    dname =  'bs_m2l2p_meas_06232020.mat'
    dfile = path.join(path.split(__file__)[0], dname)
    ofile = tpath_join(path.splitext(dname)[0] + '_fit.mat')
    pfile = tpath_join(path.splitext(dname)[0] + '_fit.pdf')
    args = '{} {} -Xc xfer -F F_Hz --plot_fit {} -C "coh.L2P, cohP2P" --SNR_min 2 --mode full2x --choose baseline --never_unstable_poles'.format(dfile, ofile, pfile)
    pprint('calling with args')
    pprint(args)
    IIRrationalV2fit(args)
    return

def test_l2p_fit_COH_refine(tpath_join, tpath_preclear, pprint):
    dname =  'bs_m2l2p_meas_06232020.mat'
    dfile = path.join(path.split(__file__)[0], dname)
    ofile = tpath_join(path.splitext(dname)[0] + '_fit.mat')
    pfile = tpath_join(path.splitext(dname)[0] + '_fit.pdf')
    args = '{} {} -Xc xfer -F F_Hz --plot_fit {} -C "coh.L2P, cohP2P" --SNR_min 1 --mode full --order 10 --choose baseline --never_unstable_poles --zeros=-0.06620+0.34045j,-0.06620-0.34045j,0.00468+0.91859j,0.00468-0.91859j,0.87592,-0.84453 --poles=-0.07553+0.29451j,-0.07553-0.29451j,-0.02012+1.23408j,-0.02012-1.23408j,-0.01123+0.50059j,-0.01123-0.50059j'.format(dfile, ofile, pfile)
    pprint('calling with args')
    pprint(args)
    IIRrationalV2fit(args)
    return
