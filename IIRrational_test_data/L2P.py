"""
Contains setup functions for test data, these are returned in a declarative.Bunch dictionary, with some annotation about the number of data sets
"""
from os import path

import numpy as np

from IIRrational.testing import utilities
from IIRrational import testing


data_folder = path.join(path.split(__file__)[0], 'data/L2P')

description = """
LIGO data for Length 2 Pitch decoupling. A good example of a filter which
requires overlay AC coupling.
"""

def L2P_M0P_L3P(**kwargs):
    """
    """
    dset = testing.testcase2data(path.join(data_folder, 'M0P_L3P.mat'))
    #from a particularly nice fit. Probably could be yet lower order though
    return utilities.generator_autofill(
        F_Hz               = dset.F_Hz,
        data               = dset.data,
        SNR                = dset.SNR,
        F_nyquist_Hz       = 1024,
        **kwargs
    )


def L2P_GNDL_L3P(**kwargs):
    """
    """
    dset = testing.testcase2data(path.join(data_folder, 'GNDL_L3P.mat'))
    #from a particularly nice fit. Probably could be yet lower order though
    return utilities.generator_autofill(
        F_Hz               = dset.F_Hz,
        data               = dset.data,
        SNR                = dset.SNR,
        F_nyquist_Hz       = 1024,
        **kwargs
    )


def L2P_MULT(**kwargs):
    """
    """
    dset1 = testing.testcase2data(path.join(data_folder, 'M0P_L3P.mat'))
    dset2 = testing.testcase2data(path.join(data_folder, 'GNDL_L3P.mat'))
    assert(np.all(dset1.F_Hz == dset2.F_Hz))
    #from a particularly nice fit. Probably could be yet lower order though
    return utilities.generator_autofill(
        F_Hz               = dset1.F_Hz,
        data               = dset2.data / dset1.data,
        SNR                = 1/(1/dset1.SNR + 1/dset1.SNR),
        F_nyquist_Hz       = 1024,
        **kwargs
    )

def L2P_MULT_WLF(**kwargs):
    """
    Weights the low frequencies more
    """
    dset1 = testing.testcase2data(path.join(data_folder, 'M0P_L3P.mat'))
    dset2 = testing.testcase2data(path.join(data_folder, 'GNDL_L3P.mat'))
    assert(np.all(dset1.F_Hz == dset2.F_Hz))
    #from a particularly nice fit. Probably could be yet lower order though
    select = dset1.F_Hz > 0
    return utilities.generator_autofill(
        F_Hz         = dset1.F_Hz[select],
        data         = (dset2.data / dset1.data)[select],
        SNR          = (abs(1 / (.01 + 1j * dset1.F_Hz))**.5 + 1/(1/dset1.SNR + 1/dset1.SNR))[select],
        F_nyquist_Hz = 1024,
        **kwargs
    )


datasets = dict(
    L2P_M0P_L3P = utilities.make_description(
        generator = L2P_M0P_L3P,
        instances = 1,
        description = description,
    ),
    L2P_GNDL_L3P = utilities.make_description(
        generator = L2P_GNDL_L3P,
        instances = 1,
        description = description,
    ),
    L2P_MULT = utilities.make_description(
        generator = L2P_MULT,
        instances = 1,
        description = description,
    ),
    L2P_MULT_WLF = utilities.make_description(
        generator = L2P_MULT_WLF,
        instances = 1,
        description = description,
    )
)
