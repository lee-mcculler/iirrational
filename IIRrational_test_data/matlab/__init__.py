# -*- coding: utf-8 -*-
"""
"""
from __future__ import division, print_function, unicode_literals

import os

fpath, _ = os.path.split(__file__)

matfiles = dict()

for fname in os.listdir(fpath):
    fbase, fext = os.path.splitext(fname)
    if fext.lower() in ['.mat', '.txt', '.csv']:
        matfiles[fname] = os.path.join(fpath, fname)
