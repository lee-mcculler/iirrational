

F_Hz = linspace(0, 100, 100);
xfer = 10 ./ (1 + 10j ./ F_Hz);
SNR = abs(10 ./ (1 + 30j ./ F_Hz));
noneC = {};
noneA = [];

str.A = 1;

data.xfer = xfer;
data.F_Hz = F_Hz;
data.SNR = SNR;
data.noneC = {};
data.noneA = [];
data.confuse = {[1,2], {1,2}, str};

save('iir_test_vars.mat', 'F_Hz', 'xfer', 'SNR', 'noneA', 'noneC');
save('iir_test_struct.mat', 'data');

dataRI.Xr   = real(xfer);
dataRI.Xi   = imag(xfer);
dataRI.freq = F_Hz;
dataRI.SNR  = SNR;

save('iir_test_struct_RI.mat', 'dataRI');
