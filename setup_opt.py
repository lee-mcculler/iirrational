#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division, print_function, unicode_literals
from setup import setup_args


def configuration(parent_package='', top_path=None):
    from numpy.distutils.misc_util import Configuration
    config = Configuration(None, parent_package, top_path)
    config.set_options(
        ignore_setup_xxx_py=True,
        assume_default_configuration=True,
        delegate_options_to_subpackages=True,
        quiet=True
    )
    config.add_subpackage('IIRrational')
    return config


if __name__ == "__main__":
    from numpy.distutils.core import setup
    setup(
        configuration = configuration,
        **setup_args
    )
