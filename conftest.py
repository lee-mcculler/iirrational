# -*- coding: utf-8 -*-
"""
"""
from __future__ import division, print_function, unicode_literals

from IIRrational.pytest import (
    relfile_test,
    pytest_addoption,
    plot,
    tpath,
    tpath_join,
    tpath_preclear,
    test_trigger,
    ic,
    browser,
    plotsections,
    plot_verbosity,
)


### PUT in individual test files
#slow = pytest.mark.skipif(
#    not pytest.config.getoption("--do-benchmarks"),
#    reason="need --do-benchmarks option to run"
#)
